import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CommonTableComponent } from './common-table/common-table.component';
import { DashBoardComponent } from './dashboard/dashboard.component';

const routes: Routes = [
  { path: 'common-table', component: CommonTableComponent },
  { path: '', component: DashBoardComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
