export default class Customer {
    id: string;
    firstName: string;
    lastName: string;
    address: string;
    dateOfBirth: Date;
    gender: number;
    provinceCode: string;
    districtCode: string;
    wardCode: string;
    balanceAmount: number;
}