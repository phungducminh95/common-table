import { IGenericDataType } from './datatype';
import  * as moment from 'moment';

export class DateDataType implements IGenericDataType<string> {
	format(value: string): string {
		if (value) {
			return moment(value, this.sourceFormat).format(this.destFormat);
		}
		return "";
	}

	private sourceFormat: string;
	private destFormat: string;

	constructor(sourceFormat: string, destFormat: string) {
		this.sourceFormat = sourceFormat;
		this.destFormat = destFormat;
	}
}
