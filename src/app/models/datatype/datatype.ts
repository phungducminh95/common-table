export interface IGenericDataType<T> {
    format(value: T): string;
}
