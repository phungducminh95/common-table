import { IGenericDataType } from './datatype';
export class NumericDataType implements IGenericDataType<number> {
    format(value: number): string {
        return value.toString();
    }
}
