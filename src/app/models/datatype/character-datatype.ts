import { IGenericDataType } from './datatype';

export class CharacterDataType implements IGenericDataType<string> {
	format(value: string): string {
		if (value) {
			return value.toString();
		}

		return "";
	}
	maxLength?: number;
}
