import { IGenericDataType } from './datatype';
import { Gender } from '../enumeration';

/**
 * For now, we only support gender first
 * We'll support other enum later on the track
 */
export class EnumDataType implements IGenericDataType<number> {
  format(value: number): string {
    // https://stackoverflow.com/questions/18111657/how-does-one-get-the-names-of-typescript-enum-entries
    return Gender[<Gender> value];
  }
}

