import { IGenericDataType } from './datatype';

export class DependenceDataType implements IGenericDataType<any> {
  format(value: any): string {
    return this.callback(value, value, value);
  }

  private callback: ((provinceCode: string, districtCode: string, wardCode: string) => string);

  constructor(callback: ((provinceCode: string, districtCode: string, wardCode: string) => string)) {
    this.callback = callback;
  }
}