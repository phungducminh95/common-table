import { CommonTableColumnModel, ICommonTableColumnModel } from './common-table-column.model';
import { ICommonTableColumnOption } from './common-table-column-option.model';
export class ActionBaseCommonTableColumnOption implements ICommonTableColumnOption {
  constructor(public name: string, public callBack: ((datum: any) => string)) {
  }
  createColumnModel(datum: any): ICommonTableColumnModel {
    return new CommonTableColumnModel(this.callBack(datum));
  }
}
