import { CommonTableLoadingColumnModel, ICommonTableColumnModel } from './common-table-column.model';
import { ICommonTableColumnOption } from './common-table-column-option.model';

export class AsyncLoadingCommonTableColumnOption implements ICommonTableColumnOption {
  constructor(public name: string) {
  }
  createColumnModel(datum: any): ICommonTableColumnModel {
    return new CommonTableLoadingColumnModel();
  }
}
