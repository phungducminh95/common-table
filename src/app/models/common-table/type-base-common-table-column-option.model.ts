import { IGenericDataType } from '../datatype/datatype';
import { CommonTableColumnModel, ICommonTableColumnModel } from './common-table-column.model';
import { ICommonTableColumnOption } from './common-table-column-option.model';
export class TypeBaseCommonTableColumnOption<T> implements ICommonTableColumnOption {
  createColumnModel(datum: any): ICommonTableColumnModel {
    return new CommonTableColumnModel(this.dataType.format(datum[this.field]));
  }
  constructor(public name: string, public field: string, public dataType: IGenericDataType<T>) {
  }
}
