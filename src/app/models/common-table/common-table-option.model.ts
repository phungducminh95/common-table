import { ICommonTableColumnOption } from './common-table-column-option.model';

export default class CommonTableOption {
    insert: boolean;
    search: boolean;
    columnOptions: ICommonTableColumnOption[];

    constructor(columnOptions: ICommonTableColumnOption[], insert?: boolean, search?: boolean) {
        this.columnOptions = columnOptions;
        this.insert = insert;
        this.search = search;
    }
}

