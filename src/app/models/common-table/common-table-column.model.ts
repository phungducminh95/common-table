export interface ICommonTableColumnModel {
  content: string;
}

export class CommonTableColumnModel implements ICommonTableColumnModel {
  content: string;

  constructor(content: string) {
    this.content = content;
  }
}

export class CommonTableLoadingColumnModel implements ICommonTableColumnModel {
  content: string = `<div class="lds-ellipsis"><div></div><div></div><div></div><div></div></div>`;
}