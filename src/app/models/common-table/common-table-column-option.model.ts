import { ICommonTableColumnModel } from './common-table-column.model';

export interface ICommonTableColumnOption {
  name: string;
  createColumnModel(datum: any): ICommonTableColumnModel;
}
