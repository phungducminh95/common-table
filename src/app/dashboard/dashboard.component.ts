import { Component, OnInit } from '@angular/core';
import Customer from '../models/customers';
import customterData from '../sample-data/customers-data';
import { CharacterDataType } from '../models/datatype/character-datatype';
import { DateDataType } from '../models/datatype/date-datatype';
import { CurrencyDataType } from '../models/datatype/currency-datatype';
import { EnumDataType } from '../models/datatype/enum-datatype';
import { ActionBaseCommonTableColumnOption } from '../models/common-table/action-base-common-table-column-option.model';
import { AsyncLoadingCommonTableColumnOption } from '../models/common-table/async-loading-common-table-column-option.model';
import { TypeBaseCommonTableColumnOption } from '../models/common-table/type-base-common-table-column-option.model';
import CommonTableOption from '../models/common-table/common-table-option.model';

@Component({
  selector: 'dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashBoardComponent implements OnInit {

  isEmptyOption: boolean = true;

  ngOnInit(): void {
    // an simple hack to make it look like a async call
    setInterval(() => {
      // !!Note: this seem to be quite overhead when we change the entire tableOption just to made the change for Address
      // But I think in current situation, the current implementation should be okay for now
      // If there is performace issues later, we will tweak things a little bit :D

      // do this way to make a loop for you to see the change
      if (this.isEmptyOption) {
        this.tableOption = this.getFullOption();
      } else {
        this.tableOption = this.getEmptyAddressOption();
      }
      this.isEmptyOption = !this.isEmptyOption;

    }, 5000);
  }

  customers: Customer[];
  tableOption: CommonTableOption;

  constructor() {
    this.customers = customterData;
    this.isEmptyOption = true;
    this.tableOption = this.getEmptyAddressOption();
  }

  private getEmptyAddressOption(): CommonTableOption {
    let columnOptions = [
      new TypeBaseCommonTableColumnOption<string>("ID", "id", new CharacterDataType()),
      new TypeBaseCommonTableColumnOption<string>("First Name", "firstName", new CharacterDataType()),
      new TypeBaseCommonTableColumnOption<string>("Last Name", "lastName", new CharacterDataType()),
      new AsyncLoadingCommonTableColumnOption("Address"),
      new TypeBaseCommonTableColumnOption<string>("Date Of Birth", "dateOfBirth", new DateDataType("DD/MM/YYYY", "DD MMMM YYYY")),
      new TypeBaseCommonTableColumnOption<number>("Gender", "gender", new EnumDataType()),
      new TypeBaseCommonTableColumnOption<number>("Balance Amount", "balanceAmount", new CurrencyDataType("VND", "VietNamDong")),
    ]

    return new CommonTableOption(columnOptions, false, false);
  }

  private getFullOption(): CommonTableOption {
    let generateAddressFunc = ((customer: Customer): string => {
      // In this case you should pass your location data and customer info match together to get address text
      // Since I so lazy, I just create simple text base on customer provinceCode, districtCode, wardCode 
      return "Province: " + customer.provinceCode + " District: " + customer.districtCode + " Ward: " + customer.wardCode;
    });

    let columnOptions = [
      new TypeBaseCommonTableColumnOption<string>("ID", "id", new CharacterDataType()),
      new TypeBaseCommonTableColumnOption<string>("First Name", "firstName", new CharacterDataType()),
      new TypeBaseCommonTableColumnOption<string>("Last Name", "lastName", new CharacterDataType()),

      // Wow! Angular is great
      // When I use Vue, I should watch the props (aka Input in Angular world) changes to rerender the table component.
      // In Angular, just change the Input and Bump! Anything rerender :D
      new ActionBaseCommonTableColumnOption("Address", generateAddressFunc),
      new TypeBaseCommonTableColumnOption<string>("Date Of Birth", "dateOfBirth", new DateDataType("DD/MM/YYYY", "DD MMMM YYYY")),
      new TypeBaseCommonTableColumnOption<number>("Gender", "gender", new EnumDataType()),
      new TypeBaseCommonTableColumnOption<number>("Balance Amount", "balanceAmount", new CurrencyDataType("VND", "VietNamDong")),
    ]

    return new CommonTableOption(columnOptions, false, false);
  }

  handleSearch(payload: string) {
    console.log(payload);
  }

  handleSearchEnter(payload: string) {
    console.log("Enter: " + payload);
  }

  handleInsertButtonClicked() {
    console.log("Add Clicked");
  }
}
