import { Component, Input, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { ICommonTableColumnModel } from '../models/common-table/common-table-column.model';
import { ICommonTableColumnOption } from '../models/common-table/common-table-column-option.model';
import CommonTableOption from '../models/common-table/common-table-option.model';

@Component({
  selector: 'common-table',
  templateUrl: './common-table.component.html',
  styleUrls: ['./common-table.component.scss']
})
export class CommonTableComponent implements OnChanges {
  @Input() data: any[];
  
  @Input() option: CommonTableOption;

  @Output() insert = new EventEmitter();

  @Output() search = new EventEmitter<string>();

  @Output() searchEnter = new EventEmitter<string>();

  formatField(columnOption: ICommonTableColumnOption, datum: any): ICommonTableColumnModel {
    return columnOption.createColumnModel(datum);
  }

  ngOnChanges(changes: SimpleChanges): void {
    console.log("OnChanged");
  }

  onInsertClicked() {
    this.insert.emit();
  }

  onQuery(value: string) {
    this.search.emit(value);
  }

  onQueryEnter(value: string) {
    this.searchEnter.emit(value);
  }
}
